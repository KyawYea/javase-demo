import java.time.LocalDate;
import java.time.Year;
import java.time.YearMonth;

public class CheckLeapYear {

	public static void main(String[] args) {
		//We will start with LocalDate
		
		//Firstly we will define already date.
		
		String date = "2020-05-25";
		boolean problem1  = LocalDate.parse(date).isLeapYear();
		
		if(problem1) {
			System.out.println(date+" is leap year");
		}else {
			System.out.println(date+" is not leap year");
		}
		//Ok, now will use LocalDate.now()
		
		boolean problem2 = LocalDate.now().isLeapYear();
		int year = LocalDate.now().getYear();
		if(problem2) {
			System.out.println(year+" is leap year");
		}else {
			System.out.println(year+" is not leap year");
		}
		
		//Ok,now will use YearMonth.now()
		
		boolean problem3 = YearMonth.now().isLeapYear();
		int ymYear = YearMonth.now().getYear();
		if(problem3) {
			System.out.println(ymYear+" is leap year");
		}else {
			System.out.println(ymYear+" is not leap year");
		}
		
		//Ok,now will define already YearMonth.
		
		String yearMonth = "2020-02";
		boolean problem4 = YearMonth.parse(yearMonth).isLeapYear();
		
		if(problem4) {
			System.out.println(yearMonth+" is leap year");
		}else {
			System.out.println(yearMonth+" is not leap year");
		}
		
		//Ok, now finally we will use Year
		
		boolean problem5 =Year.now().isLeap();
		if(problem5) {
			System.out.println(Year.now()+" is leap year");
		}else {
			System.out.println(Year.now()+" is not leap year");
		}
		
		//Ok,now Let's run.
	}

}
